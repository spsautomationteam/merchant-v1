@Application_Tempates
Feature: Application Tempates test
    I Want to test all Templates endpoints available for Merchant OnBoarding    
	
	##################   Templates_GetAddons    ####################
	
	@templates_GetAddOns_byPartIdAndPartProvider
    Scenario: Verify 'Get AddOns' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?PartProvider=Merchant&PartId=Gateway
        Then response code should be 200
	#	And response body should contain "title":"FANF CP/CNP
	
	
	@templates_GetAddOns_byPartIdGateway
    Scenario Outline: Verify 'Get AddOns' Template with Gateway
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partProvider=<PartProvider>&PartId=Gateway
        Then response code should be 200
	#	And response body should contain "title":"FANF CP/CNP

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |		
		

	@templates_GetAddOns_byPartIdSoftware
    Scenario Outline: Verify 'Get AddOns' Template with Software
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partProvider=<PartProvider>&PartId=Software
        Then response code should be 200
	#	And response body should contain "title":"FANF CP/CNP

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
	
	@templates_GetAddOns_byPartIdTerminal
    Scenario Outline: Verify 'Get AddOns' Template with Terminal
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partProvider=<PartProvider>&PartId=Terminal
        Then response code should be 200
	#	And response body should contain "title":"FANF CP/CNP

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
		
	@templates_GetAddOns_byPartIdTerminalPrinter
    Scenario Outline: Verify 'Get AddOns' Template with Terminal/Printer
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partProvider=<PartProvider>&PartId=Terminal/Printer
        Then response code should be 200
	#	And response body should contain "title":"FANF CP/CNP

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
		
#--------------- Negative Scenarios -------------------------
		
	@templates_GetAddOns_byPartIdBlank_Parameterize
    Scenario Outline: Verify 'Get AddOns' Template with Terminal
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partProvider=<PartProvider>
        Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
		
	@templates_GetAddOns_byPartProviderBlank_Parameterize
    Scenario Outline: Verify 'Get AddOns' Template with Terminal
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partId=<PartId>
        Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

		Examples:
		|	PartId			    |
		|	Software	        |
		|	Terminal			|
		|	Terminal/Printer    |	
		|	Gateway      	    |
		
	@templates_GetAddOns_byBlankPartId
    Scenario: Verify 'Get AddOns' Template with Blank PartIdBlank
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?PartProvider=ISO
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

	@templates_GetAddOns_byBlankPartProvider
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons?partId=Gateway
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

	@templates_GetAddOns_byBlankPartProviderAndPartId
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	
		
	@templates_GetAddOns_byInvalidPartProvider
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/addons??PartProvider=TEST&partId=Gateway
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	
	
		
	##################   Templates_GetAdvanceFundingProcessors    ####################	

	@templates_GetAdvanceFundingProcessors
    Scenario: Verify 'Get Advance Funding Processors' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/advancefundingprocessors
        Then response code should be 200
	#	And response body should contain "title":"AdvanceMe"
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
		
	##################   Templates_GetAssociations    ####################			
		
	@templates_GetAssociations
    Scenario: Verify 'Get Associations' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/associations
        Then response code should be 200
	#	And response body should contain "title":"DEMO NET1"
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
#	@templates_GetBackEndProcessors
#    Scenario: Verify 'Get Back-End Processors' Template
#        Given I have valid Templates request data        
#        When I get data for Templates /Templates/BackEndProcessors
#        Then response code should be 200		
#		And response body should contain "title":"Vital"


	##################   Templates_GetDiscountPaidFrequencies    ####################	
		
	@templates_GetDiscountPaidFrequencies
    Scenario: Verify 'Get Discount Paid Frequencies' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/discountpaidfrequencies
        Then response code should be 200
		And response body should contain "title":"Monthly"
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
		
	##################   Templates_GetEquipment    ####################			
	
	@templates-GetEquipment_byPartTypeAndPartProvider
    Scenario: Verify 'Get AddOns' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?PartProvider=Merchant&PartType=Gateway
        Then response code should be 200
		And response body should contain "partId"
		And response body should contain "manufacturerName"
		And response body should contain "additionalDetails"
		And response body should contain "chargeAmount"
	
	
	@templates-GetEquipment_byPartType_Gateway
    Scenario Outline: Verify 'Get AddOns' Template with Gateway
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partType=Gateway&partProvider=<PartProvider>
        Then response code should be 200
		And response body should contain "partId"
		And response body should contain "manufacturerName"
		And response body should contain "additionalDetails"
		And response body should contain "chargeAmount"

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |		
		

	@templates-GetEquipment_byPartType_Software
    Scenario Outline: Verify 'Get AddOns' Template with Software
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partType=Software&partProvider=<PartProvider>
        Then response code should be 200
		And response body should contain "partId"
		And response body should contain "manufacturerName"
		And response body should contain "additionalDetails"
		And response body should contain "chargeAmount"

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
	
	@templates-GetEquipment_byPartType_Terminal
    Scenario Outline: Verify 'Get AddOns' Template with Terminal
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partType=Terminal&partProvider=<PartProvider>
        Then response code should be 200
		And response body should contain "partId"
		And response body should contain "manufacturerName"
		And response body should contain "additionalDetails"
		And response body should contain "chargeAmount"

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
		
	@templates-GetEquipment_byPartType_TerminalPrinter
    Scenario Outline: Verify 'Get AddOns' Template with Terminal/Printer
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partType=TerminalOrPrinter&partProvider=<PartProvider>
        Then response code should be 200
		And response body should contain "partId"
		And response body should contain "manufacturerName"
		And response body should contain "additionalDetails"
		And response body should contain "chargeAmount"

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
		
#--------------- Negative Scenarios -------------------------
		
	@templates-GetEquipment_byPartTypeBlank_Parameterize
    Scenario Outline: Verify 'Get AddOns' Template with Terminal
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partProvider=<PartProvider>
        Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

		Examples:
		|	PartProvider	    |
		|	Merchant	        |
		|	SagePaymentSolutions|
		|	ISO				    |	
		|	SageSouthEast	    |
		
	@templates-GetEquipment_byPartProviderBlank_Parameterize
    Scenario Outline: Verify 'Get AddOns' Template with Terminal
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partType =<PartType>
        Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

		Examples:
		|	PartType 			|
		|	Software	        |
		|	Terminal			|
		|	Terminal/Printer    |	
		|	Gateway      	    |
		
	@templates-GetEquipment_byBlankPartType 
    Scenario: Verify 'Get AddOns' Template with Blank PartType Blank
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?PartProvider=ISO
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

	@templates-GetEquipment_byBlankPartProvider
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment?partType =Gateway
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	

	@templates-GetEquipment_byBlankPartProviderAndPartType 
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	
		
	@templates-GetEquipment_byInvalidPartProvider
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment??PartProvider=TEST&partType =Gateway
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	
		
	@templates-GetEquipment_byInvalidPartType
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment??PartProvider=ISO&partType=TEST
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	
		
	@templates-GetEquipment_byFieldsDataReverse
    Scenario: Verify 'Get AddOns' Template with Blank PartProvider
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipment??PartProvider=Gateway&partType=Merchant
		Then response code should be 404
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be No HTTP resource was found that matches the request	
	
	
	
	##################   Templates-GetEquipmentPrograms    ####################	
	
	@templates-GetEquipmentPrograms
    Scenario: Verify 'Get Equipment Programs/Features' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/equipmentprograms?PartId=CardReader
        Then response code should be 200
	#	And response body should contain "programName":"APRIVA / EXADIGM"
	

    ##################   Templates_GetFanfTypes   ####################
	
	@templates_GetFanfTypes
    Scenario: Verify 'Get Fanf Types' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/fanftypes
		Then response code should be 200
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
	##################   Templates_GetFanfTypes   ####################
		
	@templates_GetFees
    Scenario: Verify 'Get Fees' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/fees
        Then response code should be 200
		And response body should contain "product"
		And response body should contain "fees"
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "category"
		And response body should contain "categoryId"
		And response body should contain "systemDefaultValue"
		And response body should contain "expectedValue"
		And response body should contain "isSystemDefault"
		
	##################   Templates_GetFrontEndProcessors   ####################
		
#	@templates_GetFrontEndProcessors
#    Scenario: Verify 'Get Front-End Processors' Template
#        Given I have valid Templates request data        
#        When I get data for Templates /Templates/FrontEndProcessors
#        Then response code should be 200
#		And response body should contain "title":"Paymentech"

	##################   Templates_GetLeadSources   ####################
		
	@templates_GetLeadSources
    Scenario: Verify 'Get Lead Sources' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/leadsources
        Then response code should be 200
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
		
	##################   Templates_GetLeasingCompanies   ####################
		
		
#	@templates_GetLeasingCompanies
#    Scenario: Verify 'Get Leasing Companies' Template
#        Given I have valid Templates request data        
#        When I get data for Templates /templates/leasingcompanies
#        Then response code should be 200
#		And response body should contain "title":"Ladco"


	##################   Templates_GetPINDebitInterchangeTypes  ####################
		
	@templates_GetPINDebitInterchangeTypes
    Scenario: Verify 'Get PIN Debit Interchange' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/pindebitinterchangetypes
        Then response code should be 200
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
		
	##################   Templates_GetProducts  ####################

	@templates_GetProducts
    Scenario: Verify 'Get Products' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/products
        Then response code should be 200
		And response body should contain "services"	
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"
		
		
	##################   Templates_GetReferralGroups  ####################
		
	@templates_GetReferralGroups
    Scenario: Verify 'Get Referral Groups' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/referralgroups
        Then response code should be 200
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"		
		
		
	##################   Templates_GetSettlementTypes  ###################
			
	@templates_GetSettlementTypes
    Scenario: Verify 'Get Settlement Types' Template
        Given I have valid Templates request data        
        When I get data for Templates /templates/settlementtypes
        Then response code should be 200
		And response body should contain "title"
		And response body should contain "id"
		And response body should contain "isDefault"
		And response body should contain "canEdit"		


		