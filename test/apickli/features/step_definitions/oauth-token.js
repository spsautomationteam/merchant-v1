/* jslint node: true */
'use strict';

// Create a new singleton
var hmacTools = new (require("../../../hmacTools.js"))();
//var async = require('async');
var factory = require('./factory.js');

var contentType = "application/json";

module.exports = {

	createToken: function (apickli, callback) {

		var localApiclli = factory.getNewApickliInstance(apickli.scenarioVariables.oauthBasepath);

		var pathSuffix = "/tokens";
		var url = localApiclli.domain + pathSuffix;
		var reqString =
			'{"grant_type":"' + apickli.scenarioVariables.oauthGrantType + '","identity":"' + apickli.scenarioVariables.oauthIdentity + '","client_id":"' + apickli.scenarioVariables.clientId + '"}';

		var body = reqString; //JSON.stringify(reqString);
		localApiclli.setRequestBody(body);
		var nonce = hmacTools.nonce(12);
		var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.createHmac(clientSecret, "POST" + url + body + nonce + timestamp);// "POST", url, body, '', nonce, timestamp);
		
		localApiclli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
		localApiclli.addRequestHeader('nonce', nonce);
		localApiclli.addRequestHeader('timestamp', timestamp);
		localApiclli.addRequestHeader('Authorization', hmac);
		localApiclli.addRequestHeader('Content-Type', 'application/json');

		localApiclli.post(pathSuffix, function (err, response) {

			localApiclli.storeValueOfResponseBodyPathInScenarioScope('$.access_token', 'access_token');
			localApiclli.storeValueOfResponseBodyPathInScenarioScope('$.token_type', 'token_type');
			apickli.storeValueInScenarioScope('oauth.access_token', localApiclli.scenarioVariables.access_token);
			apickli.storeValueInScenarioScope('oauth.token_type', localApiclli.scenarioVariables.token_type);
			apickli.storeValueInScenarioScope("AuthorizationValue", localApiclli.scenarioVariables.token_type + ' ' + localApiclli.scenarioVariables.access_token);
			callback(response, apickli);
		});

	}
}