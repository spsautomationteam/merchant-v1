/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var invalidClientId = config.merchant.invalidClientId;
var clientId = config.merchant.clientId;
var clientSecret = config.merchant.clientSecret;

var contracterId = config.merchant.contracterId;
var invalidContractorId = config.merchant.invalidContractorId;

var oauthBasepath = config.merchant.oauth_basepath;
var oauthIdentity = config.merchant.oauth_identity;
var oauthGrantType = config.merchant.oauth_grant_type;

var oauthToken = config.merchant.oauth_token;

module.exports = function () {
    // cleanup before every scenario
    this.Before(function (scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("invalidClientId", invalidClientId);
        this.apickli.storeValueInScenarioScope("clientId", clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", clientSecret);

        this.apickli.storeValueInScenarioScope("contracterId", contracterId);
        this.apickli.storeValueInScenarioScope("invalidContractorId", invalidContractorId);

        this.apickli.storeValueInScenarioScope("oauthBasepath", oauthBasepath);
        this.apickli.storeValueInScenarioScope("oauthIdentity", oauthIdentity);
        this.apickli.storeValueInScenarioScope("oauthToken", oauthToken);
        this.apickli.storeValueInScenarioScope("oauthGrantType", oauthGrantType);
        callback();
    });
};

