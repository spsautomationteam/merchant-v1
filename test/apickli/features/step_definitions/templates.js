/* jslint node: true */
'use strict';
var apickli = require('apickli');
var factory = require('./factory.js');
var config = require('../../config/config.json');
var oauthToken = require('./oauth-token.js');

var Contenttype = "application/json";
var AuthorizationValue;
var SPSUserValue;

var prettyJson = require('prettyjson');



var getTemplateData = function (apickli, getTemplateAction, callback) {
	var pathSuffix = getTemplateAction;

	var url = apickli.domain + pathSuffix;
	console.log('\n Authorization: ' + apickli.scenarioVariables.AuthorizationValue)
	apickli.addRequestHeader('Authorization', apickli.scenarioVariables.AuthorizationValue);
	apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
	apickli.get(pathSuffix, function (err, response) {
		if (response) {
			console.log(response.body);
		}
		callback();
	});
};

module.exports = function () {

	//oauthToken.createToken(this.apickli);
	this.Given(/^I have valid Templates request data$/, function (callback) {
		oauthToken.createToken(this.apickli, function (err, response) {
			callback();
		});


	});

	this.When(/^I get data for Templates (.*)$/, function (getTemplateAction, callback) {
		getTemplateData(this.apickli, getTemplateAction, callback);
	});

	this.Given(/^I enter invalid contractorId request data$/, function (callback) {
		//this.apickli.storeValueInScenarioScope("contractorId", this.apickli.scenarioVariables.invalidContractorId);
		this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.oauthGrantType + ' ' + this.apickli.scenarioVariables.oauthToken);

		callback();

	});

	this.Given(/^I enter invalid Authorization request data$/, function (callback) {
		// this.apickli.storeValueInScenarioScope("contractorId", this.apickli.scenarioVariables.contracterId);
		this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.oauthGrantType + 'xxxxx' + this.apickli.scenarioVariables.oauthToken);
		callback();

	});



};

var prettyPrintJson = function (json) {
	var output = {
		stepContext: stepContext,
		testOutput: json
	};

	return prettyJson.render(output, {
		noColor: true
	});
};
